import { ISong } from "../song/song.model.js";

export interface IArtist {
    _id?: string,
    name: string,
    nickname: string,
    songs: [ISong];
}

