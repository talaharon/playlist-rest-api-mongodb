import { Request, Response } from "express";
import DBException from "../exceptions/DB.exception.js";
import EntityNotExistException from "../exceptions/EntityNotExist.exception.js";
import * as playlist_service from "../modules/playlist/playlist.service.js";
class ArtistController {
    async getPlaylists(req: Request, res: Response) {
        const playlists = await playlist_service.get_all_playlists();
        if (!playlists) {
            throw new EntityNotExistException("Couldn't get table 'Playlist'");
        }
        res.status(200).json(playlists);
    }

    async getPlaylistById(req: Request, res: Response) {
        const playlist = await playlist_service.get_playlist_by_id(
            req.params.id
        );
        if (!playlist) {
            throw new EntityNotExistException(
                "Couldn't find Playlist with ID " + req.params.id
            );
        } // Throw error
        res.status(200).json(playlist);
    }

    async addPlaylist(req: Request, res: Response) {
        const playlist = await playlist_service.create_playlist(req.body);
        if (!playlist) {
            throw new DBException("Failed to add playlist");
        }
        res.status(200).json(playlist);
    }

    async addSongToPlaylist(req: Request, res: Response) {
        const playlist = await playlist_service.add_song_to_playlist(
            req.params.playlistId,
            req.params.songId
        );
        if (!playlist) {
            throw new DBException("Failed to add song to playlist");
        }
        res.status(200).json(playlist);
    }

    async updatePlaylist(req: Request, res: Response) {
        const playlist = await playlist_service.update_playlist_by_id(
            req.params.id,
            req.body
        );
        if (!playlist) {
            throw new DBException("Failed to update playlist");
        }
        res.status(200).json(playlist);
    }

    async deletePlaylist(req: Request, res: Response) {
        const playlist = await playlist_service.delete_playlist_by_id(
            req.params.id
        );
        if (!playlist) {
            throw new DBException("Failed to delete playlist");
        }
        res.status(200).json(playlist);
    }

    async deleteSongFromPlaylist(req: Request, res: Response) {
        const playlist = await playlist_service.delete_song_from_playlist(
            req.params.playlistId,
            req.params.songId
        );
        if (!playlist) {
            throw new DBException("Failed to delete song from playlist");
        }
        res.status(200).json(playlist);
    }
}

export default new ArtistController();
