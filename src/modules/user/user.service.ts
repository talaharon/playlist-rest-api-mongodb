import { IUser } from "./user.model.js";
import user_schema from "./user.schema.js";
import * as playlist_service from "../playlist/playlist.service.js";

/**
 * Routing functions 
 */
export const get_all_users = async () => {
    const users = await user_schema.find();
    return users;
};
export const get_user_by_id = async (user_id: string) => {
    const user = await user_schema.findById(user_id);
    return user;
};

export const create_user = async (userToCreate: IUser) => {
    const user = await user_schema.create(userToCreate);
    return user;
};

export const update_user_by_id = async (user_id: string, partialUser: Partial<IUser>) => {
    const user = await user_schema.findByIdAndUpdate(user_id, partialUser, {
        new: true
    });
    return user;
};

export const delete_user_by_id = async (user_id: string) => {
    const userToReturn = await user_schema.findById(user_id);
    for(const playlistId of userToReturn.playlists){
        await playlist_service.delete_playlist_by_id(playlistId.toString());
    }
    const user = await user_schema.findByIdAndRemove(user_id);
    return user;
};

/**
 * Helpers
 */

export const delete_playlist_from_user = async (playlist_id: string) => {
    const users = await user_schema.find();
    for (const user of users) {
        const userData = await user_schema.findById(user._id);
        userData.playlists.pull(playlist_id);
        await userData.save();
    }
    return users;
};


export const add_playlist_to_user = async (user_id: string, playlist_id: string) => {
    const user = await user_schema.findById(user_id);
    user.playlists.push(playlist_id);
    user.save();
    return user;
};

export const get_user_by_email = async (email: string) => {
    const user = await user_schema.findOne({email});
    console.log(user);
    return user;
};
