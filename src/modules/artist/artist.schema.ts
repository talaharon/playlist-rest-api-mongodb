import mongoose from 'mongoose';

const { Schema, model } = mongoose;

export const ArtistSchema = new Schema({  
    name: { type: String, required: true },
    nickname: { type: String, required: true },
    songs: [{ type: Schema.Types.ObjectId, ref: 'song' }],
}, { timestamps: true });

export default model('artist', ArtistSchema);