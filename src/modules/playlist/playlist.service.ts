import playlist_schema from "./playlist.schema.js";
import * as song_service from "../song/song.service.js";
import *  as user_service from "../user/user.service.js";
import { IPlaylist } from "./playlist.model.js";
import { AnyObject } from "mongoose";


/**
 * Routing functions 
 */
export const get_all_playlists = async () => {
    const playlists = await playlist_schema.find();
    return playlists;
};
export const get_playlist_by_id = async (playlist_id: string) => {
    const playlist = await playlist_schema.findById(playlist_id).populate('user');
    return playlist;
};

export const create_playlist = async (playlistToCreate: IPlaylist) => {
    const playlist = await playlist_schema.create(playlistToCreate);
    await user_service.add_playlist_to_user(playlistToCreate.user, playlist._id);
    return playlist;
};

export const update_playlist_by_id = async (playlist_id: string, partialPlaylist: Partial<IPlaylist>) => {
    const playlist = await playlist_schema.findByIdAndUpdate(playlist_id, partialPlaylist, {
        new: true,
    });
    return playlist;
};

export const delete_playlist_by_id = async (playlist_id: string) => {
    const playlist = await playlist_schema.findByIdAndRemove(playlist_id);
    if (playlist) {
        await song_service.delete_playlist_from_songs(playlist_id);
        await user_service.delete_playlist_from_user(playlist_id);
    }
    return playlist;
};

export const add_song_to_playlist = async (playlist_id: string, song_id: string) => {
    const playlist = await playlist_schema.findById(playlist_id);
    const song = await song_service.add_playlist_to_song(playlist_id, song_id);
    playlist.songs.push(song);
    playlist.save();
    return playlist;
};

export const delete_song_from_playlist = async (playlist_id: string, song_id: string) => {
    const playlist = await playlist_schema.findById(playlist_id);
    await playlist?.songs.pull(song_id);
    playlist.save();
    await song_service.delete_playlist_from_song(playlist_id, song_id);
    return playlist;
};


/**
 * Helpers
 */
export const delete_song_from_playlists = async (song_id: string) => {
    const playlists = await get_all_playlists();
    if (playlists) {
        for (const playlist of playlists) {
            await delete_song_from_playlist(playlist._id, song_id);
        }
    }
    return playlists;
};

export const update_song_in_playlists = async (song_id: string, payload: AnyObject) => {
    const playlists = await get_all_playlists();
    if (playlists) {
        for (const playlist of playlists) {
            const songs = playlist.songs;
            const index = songs.findIndex((song: AnyObject) => {
                return song._id.toString() === song_id;

            });
            if (index !== -1) {
                for (const key in payload) {
                    songs[index][key] = payload[key];
                }
                await playlist.save();
            }
        }
    }
    return playlists;
};
