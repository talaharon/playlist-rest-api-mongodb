import { Request, Response } from "express";
import DBException from "../exceptions/DB.exception.js";
import EntityNotExistException from "../exceptions/EntityNotExist.exception.js";
import * as song_service from "../modules/song/song.service.js";
class SongController {
    async getSongs(req: Request, res: Response) {
        const songs = await song_service.get_all_songs();
        if (!songs) {
            throw new EntityNotExistException("Couldn't get table 'Songs'");
        }
        res.status(200).json(songs);
    }

    async getSongById(req: Request, res: Response) {
        const song = await song_service.get_song_by_id(req.params.id);
        if (!song) {
            throw new EntityNotExistException(
                "Couldn't find song with ID " + req.params.id
            );
        }
        res.status(200).json(song);
    }

    async addSong(req: Request, res: Response) {
        const song = await song_service.create_song(req.body);
        if (!song) {
            throw new DBException("Failed to add song");
        }
        res.status(200).json(song);
    }

    async updateSong(req: Request, res: Response) {
        const song = await song_service.update_song_by_id(
            req.params.id,
            req.body
        );
        if (!song) {
            throw new DBException("Failed to update song");
        }
        res.status(200).json(song);
    }

    async deleteSong(req: Request, res: Response) {
        const song = await song_service.delete_song_by_id(req.params.id);
        if (!song) {
            throw new DBException("Failed to delete song");
        }
        res.status(200).json(song);
    }
}

export default new SongController();
