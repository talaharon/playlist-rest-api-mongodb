import mongoose from 'mongoose';
import { SongSchema } from '../song/song.schema.js';

const { Schema, model } = mongoose;

export const PlaylistSchema = new Schema({
    name: { type: String, required: true },
    user: { type: Schema.Types.ObjectId, ref: 'user', required: true},
    songs: [SongSchema],
}, { timestamps: true });

export default model('playlist', PlaylistSchema);