import express from "express";
import SongController from "../controllers/song.controller.js";
import { raw } from "../middlewares/common.middlewares.js";

const songRouter = express.Router();
// artistRouter.use(express.json())

// Get all songs
songRouter.get("/", raw(SongController.getSongs));

// Get song by ID
songRouter.get("/:id", raw(SongController.getSongById));

// Add a new song with Artist
songRouter.post("/", raw(SongController.addSong));

// Update an artist
songRouter.put("/:id", raw(SongController.updateSong));

// Delete an Artist (and all of his/her songs)
songRouter.delete("/:id", raw(SongController.deleteSong));

export default songRouter;
