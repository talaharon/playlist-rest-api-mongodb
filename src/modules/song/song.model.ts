

export interface ISong {
    _id?: string,
    name: string,
    duration: number,
    genre: string,
    artist: string;
    playlists: [];
}

