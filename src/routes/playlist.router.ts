import express from "express";
import PlaylistController from "../controllers/playlist.controller.js";
import { raw } from "../middlewares/common.middlewares.js";

const playlistRouter = express.Router();
// artistRouter.use(express.json())

// Get all playlists
playlistRouter.get("/", raw(PlaylistController.getPlaylists));

// Get playlist by ID
playlistRouter.get("/:id", raw(PlaylistController.getPlaylistById));

// Add a new playlist
playlistRouter.post("/", raw(PlaylistController.addPlaylist));

// Add a new song to playlist
playlistRouter.put(
    "/:playlistId/song/:songId",
    raw(PlaylistController.addSongToPlaylist)
);

// Update a playlist
playlistRouter.put("/:id", raw(PlaylistController.updatePlaylist));

// Delete a playlist by ID
playlistRouter.delete("/:id", raw(PlaylistController.deletePlaylist));

// Delete a song from playlist
playlistRouter.delete(
    "/:playlistId/song/:songId",
    raw(PlaylistController.deleteSongFromPlaylist)
);

export default playlistRouter;
