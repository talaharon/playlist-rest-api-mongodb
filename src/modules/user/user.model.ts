

export interface IUser {
    _id?: string,
    name: string,
    lastname: string,
    username: string,
    password: string,
    email: string,
    phone: string,
    playlists: [];
}

