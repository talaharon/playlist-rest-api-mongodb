import { Request, Response } from "express";
import DBException from "../exceptions/DB.exception.js";
import EntityNotExistException from "../exceptions/EntityNotExist.exception.js";
import * as artist_service from "../modules/artist/artist.service.js";
class ArtistController {
    async getArtists(req: Request, res: Response) {
        const artists = await artist_service.get_all_artists();
        if (!artists) {
            throw new EntityNotExistException("Couldn't get table 'Artists'");
        }
        res.status(200).json(artists);
    }

    async getArtistById(req: Request, res: Response) {
        const artist = await artist_service.get_artist_by_id(req.params.id);
        if (!artist) {
            throw new EntityNotExistException(
                "Couldn't find Artist with ID " + req.params.id
            );
        }
        res.status(200).json(artist);
    }

    async addArtist(req: Request, res: Response) {
        const artist = await artist_service.create_artist(req.body);
        if (!artist) {
            throw new DBException("Failed to add artist");
        }
        res.status(200).json(artist);
    }

    async updateArtist(req: Request, res: Response) {
        const artist = await artist_service.update_artist_by_id(
            req.params.id,
            req.body
        );
        if (!artist) {
            throw new DBException("Failed to update artist");
        }
        res.status(200).json(artist);
    }

    async deleteArtist(req: Request, res: Response) {
        const artist = await artist_service.delete_artist_by_id(req.params.id);
        if (!artist) {
            throw new DBException("Failed to delete artist");
        }
        res.status(200).json(artist);
    }
}

export default new ArtistController();
