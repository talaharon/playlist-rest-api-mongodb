import HttpException from "./Http.exception.js";

class ValidationException extends HttpException {
    constructor(msg: string) {
        super(400, msg);
    }
}

export default ValidationException;
