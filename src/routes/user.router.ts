import express from "express";
import UserController from "../controllers/user.controller.js";
import { verifyAuth } from "../middlewares/auth.middlewares.js";
import { bcryptPass, raw } from "../middlewares/common.middlewares.js";

const userRouter = express.Router();

// Make sure user is Authenticated
userRouter.use(verifyAuth);

// Get all users
userRouter.get("/all", raw(UserController.getUsers));

// Get user by ID
userRouter.get("/", raw(UserController.getUserById));

// Update a user
userRouter.put("/", bcryptPass, raw(UserController.updateUser));

// Delete a user (and all of his/her playlist)
userRouter.delete("/", raw(UserController.deleteUser));

export default userRouter;
