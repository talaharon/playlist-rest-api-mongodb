import mongoose from 'mongoose';

const { Schema, model } = mongoose;

export const UserSchema = new Schema({
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    password: {type:String, required: true},
    email: { type: String , required: true},
    phone: { type: Number, required: true},
    playlists: [{ type: Schema.Types.ObjectId, ref: 'playlist' }],
    refreshToken: {type: String}
}, { timestamps: true });




export default model('user', UserSchema);