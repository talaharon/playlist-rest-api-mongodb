import { ISong } from "./song.model.js";
import song_schema from "./song.schema.js";
import * as playlist_service from "../playlist/playlist.service.js";
import * as artist_service from "../artist/artist.service.js";
import { AnyObject } from "mongoose";


/**
 * Routing functions 
 */
export const get_all_songs = async () => {
    const songs = await song_schema.find();
    return songs;
};

export const get_song_by_id = async (song_id: string) => {
    const artist = await song_schema.findById(song_id);
    return artist;
};

export const create_song = async (songToCreate: ISong) => {
    const artist = await artist_service.get_artist_by_id(songToCreate.artist);
    if (artist) {
        const song = await song_schema.create(songToCreate);
        artist.songs.push(song._id);
        await artist.save();
        return song;
    } else {
        throw new Error("Artist does not exist");
    }

};

export const update_song_by_id = async (song_id: string, partialSong: Partial<ISong>) => {
    await playlist_service.update_song_in_playlists(song_id, partialSong);
    const song = await song_schema.findByIdAndUpdate(song_id, partialSong, {
        new: true,
    });
    return song;
};

export const delete_song_by_id = async (song_id: string) => {
    await playlist_service.delete_song_from_playlists(song_id);
    const song = await song_schema.findByIdAndRemove(song_id);
    await artist_service.delete_song_from_artist(song.artist, song_id);
    await playlist_service.delete_song_from_playlists(song_id);
    return song;
};

/**
 * Helpers 
 */
export const add_playlist_to_song = async (playlist_id: string, song_id: string) => {
    const song = await song_schema.findById(song_id);
    song.playlists.push(playlist_id);
    song.save();
    return song;
};

export const delete_playlist_from_songs = async (playlist_id: string) => {
    const songs = await song_schema.find();
    songs.forEach(async (song: AnyObject) => {
        await delete_playlist_from_song(playlist_id, song._id.toString());
    });
    return songs;
};

export const delete_playlist_from_song = async (playlist_id: string, song_id: string) => {
    const song = await song_schema.findById(song_id);
    if (song) {
        await song.playlists.pull(playlist_id);
        await song.save();
    }
    return song;
};
