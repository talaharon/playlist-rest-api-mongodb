import { IArtist } from "./artist.model.js";
import artist_scehma from "./artist.schema.js";
import * as song_service from "../song/song.service.js";

/**
 * Routing functions 
 */
export const get_all_artists = async () => {
    const artists = await artist_scehma.find();
    return artists;
};
export const get_artist_by_id = async (artist_id: string) => {
    const artist = await artist_scehma.findById(artist_id);
    return artist;
};

export const create_artist = async (artistToCreate: IArtist) => {
    const artist = await artist_scehma.create(artistToCreate);
    return artist;
};

export const update_artist_by_id = async (artist_id: string, partialArtist: Partial<IArtist>) => {
    const artist = await artist_scehma.findByIdAndUpdate(artist_id, partialArtist, {
        new: true
    });
    return artist;
};

export const delete_artist_by_id = async (artist_id: string) => {
    const artist = await artist_scehma.findByIdAndRemove(artist_id);
    for (const song of artist.songs) {
        await song_service.delete_song_by_id(song._id);
        //await playlist_service.delete_song_from_playlists(song._id);
    }
    return artist;
};

/**
 * Helpers
 */
export const delete_song_from_artist = async (artist_id: string, song_id: string) => {
    const artist = await get_artist_by_id(artist_id);
    if (artist) {
        artist.songs.pull(song_id);
        artist.save();
    }
    return artist;
};

