import express from "express";
import { not_found } from "../middlewares/errors.middlewares.js";

const router = express.Router();

//when no routes were matched...
router.use("*", not_found);

export default router;
