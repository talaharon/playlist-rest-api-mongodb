import { ISong } from "../song/song.model.js";

export interface IPlaylist {
    _id?: string,
    name: string,
    songs: [ISong];
    user: string,
}

