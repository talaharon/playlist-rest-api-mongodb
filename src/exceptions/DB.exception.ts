import HttpException from "./Http.exception.js";

class DBException extends HttpException {
    constructor(action: string) {
        super(500, "DB Internal error - Failed to use " + action);
    }
}

export default DBException;
