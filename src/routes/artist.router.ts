import express from "express";
import ArtistController from "../controllers/artist.controller.js";
import { raw } from "../middlewares/common.middlewares.js";
// import log from '@ajar/marker';
// import {raw} from '../middleware/common.middlewares.js';

const artistRouter = express.Router();
// artistRouter.use(express.json())

// Get all artists
artistRouter.get("/", raw(ArtistController.getArtists));

// Get artist by ID
artistRouter.get("/:id", raw(ArtistController.getArtistById));

// Add an artist
artistRouter.post("/", raw(ArtistController.addArtist));

// Update an artist
artistRouter.put("/:id", raw(ArtistController.updateArtist));

// Delete an Artist (and all of his/her songs)
artistRouter.delete("/:id", raw(ArtistController.deleteArtist));

export default artistRouter;
